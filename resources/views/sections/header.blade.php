<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Laravel Exercise | @yield('page_title')</title>
	
	<!-- Styles Section -->
	@section('included_styles')
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		{!! Html::style('assets/css/styles.css') !!}
	@show
	<!-- Styles Section -->
</head>
<body>


	<div id="header">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-12 col-xs-12">
					<a href="#" class="logo">LOGO</a>
				</div>
				
			</div>
		</div>
	</div><!-- header -->