@include('sections.header')

<div id="whole-container">
	
	<div id="section1" class="sections container">
		<div class="row">
			<div class="col-md-12">
				@yield('main_content')
			</div>
		</div>
	</div><!-- #section1.sections -->

</div><!--whole-container -->


@include('sections.footer')
