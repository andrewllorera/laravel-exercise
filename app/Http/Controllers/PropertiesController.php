<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;


use App\Http\Models\Property;

class PropertiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


    public function search(Property $property,Request $request){
        $property = Property::orderBy('name');

        if ($request->has('name')){
          $name = $request->input('name');
          $property->where('name', 'like', '%'.$name.'%');
        }

        if ($request->has('bedrooms')){
          $bedroomsCount = $request->input('bedrooms');
          $property->where('bedrooms', '=' , $bedroomsCount);
        }
        
        if ($request->has('bathrooms')){
          $bathroomsCount = $request->input('bathrooms');
          $property->where('bathrooms', '=' , $bathroomsCount);
        }

        if ($request->has('storeys')){
          $storeysCount = $request->input('storeys');
          $property->where('storeys', '=' , $storeysCount);
        }

        if ($request->has('garages')){
          $garagesCount = $request->input('garages');
          $property->where('garages', '=' , $garagesCount);
        }
       
        if ($request->has('price_from') && $request->has('price_to')){
            $priceFrom = $request->input('price_from');
            $priceTo = $request->input('price_to');
            $property->whereBetween('price', [$priceFrom, $priceTo]);
        }
                 

        $resultCounter = $property->count();

        if($resultCounter > 0){
            $propertyCollection['properties'] = $property->get();
            return response()->json($propertyCollection, 200);
        }else{
            //no results found
            return response()->json(false, 200);
        }
    }
}
