$(document).ready(function(){



	$( '.search-form' ).submit(function( event ) {
 
	  event.preventDefault();
	 
	  // Get some values from elements on the page:
	  var $form = $( this );
	  var postUrl = $form.attr( "action" );


	  var $inputs = $form.find("input, select, button, textarea");
	  var serializedData = $form.serialize();
	  $inputs.prop("disabled", true);
	  $('.loader').show();
	  request = $.ajax({
        url: postUrl,
        type: "post",
        data: serializedData
      });
      request.done(function (response, textStatus, jqXHR){
      	$('.results ul').html("");
      	//console.log(response['properties']);
      	if(response==false){
      		$('.results ul').append('<li>No results found</li>');
      	}else{
      		properties = response['properties'];
	      	console.log(properties);
	      	for (var i = 0; i < properties.length; i++) {
	      		$('<li>'+properties[i]['name']+'</li>').appendTo('.results ul');
	      	};
      	}
      	

      	$('.results-wrapper').fadeIn('fast');

	  });

      request.fail(function (jqXHR, textStatus, errorThrown){
        console.error(
            "An error occured: "+
            textStatus, errorThrown
        );
      });
	  request.always(function () {
	     $inputs.prop("disabled", false);
	  	 $('.loader').hide();
	  });

	});




})