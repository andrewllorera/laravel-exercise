@extends('templates.full-width')

@section('page_title', 'Home')


@section('main_content')
    
    <div class="row">
    	<div class="search-wrapper col-md-6 col-sm-12 col-xs-12">
    		<h2>Search Here:</h2>
				{!! Form::open(array('route' => 'properties.search', 'class' => 'search-form')) !!}
					<div class="input-row">
						<div class="input-label">Property Name:</div>
						<div class="input-field"><?php echo Form::text('name'); ?></div>
					</div>
					<div class="clear">
						<div class="input-row half-left">
							<div class="input-label"># of Bedrooms:</div>
							<div class="input-field"><?php echo Form::number('bedrooms', null, array('min'=>0)); ?></div>
						</div>
						<div class="input-row half-right">
							<div class="input-label"># of Bathrooms:</div>
							<div class="input-field"><?php echo Form::number('bathrooms', null, array('min'=>0)); ?></div>
						</div>
					</div>
					<div class="clear">
						<div class="input-row half-left">
							<div class="input-label"># of Storeys:</div>
							<div class="input-field"><?php echo Form::number('storeys', null, array('min'=>0)); ?></div>
						</div>
						<div class="input-row half-right">
							<div class="input-label"># of Garages:</div>
							<div class="input-field"><?php echo Form::number('garages', null, array('min'=>0)); ?></div>
						</div>
					</div>
					<div class="input-row">
						<div class="input-label">Price Range:</div>
						<div class="iBlock"> From </div>
						<div class="input-field iBlock"><?php echo Form::number('price_from', null, array('min'=>0)); ?></div>
						<div class="iBlock"> to </div>
						<div class="input-field iBlock"><?php echo Form::number('price_to', null, array('min'=>0)); ?></div>
					</div>
					<div class="input-row">
						<?php echo Form::submit('Search',array('class'=> 'btn btn-primary')); ?>
    					{!! Html::image('assets/img/ajax-loader.gif', 'Loading', array('class' => 'loader')) !!}
					</div>
				{!! Form::close() !!}
    	</div><!-- search-wrapper -->

    	<div class="results-wrapper col-md-6 col-sm-12 col-xs-12">
    		<h2>Results:</h2>
    		<div class="results">
    				<ul>
    					
    				</ul>
    		</div>
    	</div>
    </div>


@endsection