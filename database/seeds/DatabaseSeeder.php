<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(UserTableSeeder::class);
        $this->call(PropertiesTableSeeder::class);

        Model::reguard();
    }
}


class PropertiesTableSeeder extends Seeder {
    public function run() {
        DB::table('properties')->delete();
        App\Http\Models\Property::create(array('name' => 'The Victoria', 'price' => 374662,'bedrooms' => 4, 'bathrooms' => 2, 'storeys' => 2, 'garages' => 2));
        App\Http\Models\Property::create(array('name' => 'The Xavier', 'price' => 513268,'bedrooms' => 4, 'bathrooms' => 2, 'storeys' => 1, 'garages' => 2));
        App\Http\Models\Property::create(array('name' => 'The Como', 'price' => 454990,'bedrooms' => 4, 'bathrooms' => 3, 'storeys' => 2, 'garages' => 3));
        App\Http\Models\Property::create(array('name' => 'The Aspen', 'price' => 384356,'bedrooms' => 4, 'bathrooms' => 2, 'storeys' => 2, 'garages' => 2));
        App\Http\Models\Property::create(array('name' => 'The Lucretia', 'price' => 572002,'bedrooms' => 4, 'bathrooms' => 3, 'storeys' => 2, 'garages' => 2));
        App\Http\Models\Property::create(array('name' => 'The Toorak', 'price' => 521951,'bedrooms' => 5, 'bathrooms' => 2, 'storeys' => 1, 'garages' => 2));
        App\Http\Models\Property::create(array('name' => 'The Skyscape', 'price' => 263604,'bedrooms' => 3, 'bathrooms' => 2, 'storeys' => 2, 'garages' => 2));
        App\Http\Models\Property::create(array('name' => 'The Clifton', 'price' => 386103,'bedrooms' => 3, 'bathrooms' => 2, 'storeys' => 1, 'garages' => 1));
        App\Http\Models\Property::create(array('name' => 'The Geneva', 'price' => 390600,'bedrooms' => 4, 'bathrooms' => 3, 'storeys' => 2, 'garages' => 2));
    }
}